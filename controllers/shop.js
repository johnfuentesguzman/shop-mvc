const Product = require('../models/product');
const Cart = require('../models/cart');

exports.getProducts = (req, res, next) => {
  Product.fetchAll(products => {
    res.render('shop/product-list', {
      prods: products,
      pageTitle: 'All Products',
      path: '/products'
    });
  });
};

exports.getProductById = (req, res, next) => {
  const productId = req.params.productId //gettig url param
  Product.findById(productId, (product) => { // see the functions "findById" it has two parameters. the second one is a callback function as default
    res.render('shop/product-detail', { product: product , pageTitle: product.title, path: '/products'}); // redering the page and passing the data expected to show
  }); 
};
 
exports.getIndex = (req, res, next) => {
    Product.fetchAll(products => {
        res.render('shop/index', {
          prods: products,
          pageTitle: 'Shop',
          path: '/',
        });
    });
};

exports.getCart = (req, res, next) => {
    res.render('shop/cart', {
        pageTitle: 'View Cart',
        path: '/cart',
    });  
};

exports.postCart = (req, res, next) => {
  const prodId = req.body.productId;
  Product.findById(prodId, product => {
    Cart.addProduct(prodId, product.price);
  });
  res.redirect('/cart');
};

exports.getCheckout = (req, res, next) => {
    res.render('shop/checkout', {
        pageTitle: 'Checkout',
        path: '/checkout',
    });
};

exports.getOrders = (req, res, next) => {
  res.render('shop/orders', {
      pageTitle: 'Your Orders',
      path: '/orders',
  });  
};