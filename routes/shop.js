const path = require('path');

const express = require('express');

const shopController = require('../controllers/shop');

const router = express.Router();

router.get('/', shopController.getIndex); // adding the controller for handling the specific route

router.get('/products', shopController.getProducts);

router.get('/products/:productId', shopController.getProductById); // getting product by ID

router.get('/cart', shopController.getCart);

router.post('/cart', shopController.postCart); // cart POST 

router.get('/checkout', shopController.getCheckout);

router.get('/orders', shopController.getOrders);

module.exports = router;
